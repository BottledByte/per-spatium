//! This module includes all sort of systems.
//! Many of them are developer/test systems.
use crate::comp;
use crate::res;
use crate::spawn;

use bevy::prelude::*;

/// Plugin bringing in all sort of utility/developer systems and resources
pub struct GameUtilPlugin;

impl Plugin for GameUtilPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_event::<res::DeveloperCommandEvent>()
            .add_event::<res::SpawnEntityEvent>()
            .init_resource::<res::DeveloperExecutiveState>()
            .init_resource::<res::GenericSpawnEntityState>()
            .init_resource::<res::GameplaySpawnState>()
            .add_resource(res::GameBounds {
                point: Vec2::new(0.0, 0.0),
                dimension: Vec2::new(res::GAME_BOUNDS_DIMENSION_W, res::GAME_BOUNDS_DIMENSION_H),
            })
            .add_startup_system(generic_spawn_entity_startup_system.system())
            .add_system(developer_input_system.system())
            .add_system(developer_executive_system.system())
            .add_system(keep_player_in_bounds_system.system())
            .add_system(generic_spawn_entity_system.system())
            .add_system(off_bounds_cleanup_system.system())
            .add_system(gameplay_spawn_system.system())
            .add_system(check_game_over_system.system());
    }
}

/// Developer input system for creating DeveloperCommandEvents
pub fn developer_input_system(
    mut dev_events: ResMut<Events<res::DeveloperCommandEvent>>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.pressed(KeyCode::F1) {
        dev_events.send(res::DeveloperCommandEvent {
            command: "spawn_asteroid".to_string(),
        });
    }
    if keyboard_input.just_pressed(KeyCode::F2) {
        dev_events.send(res::DeveloperCommandEvent {
            command: "spawn_projectile".to_string(),
        });
    }
    if keyboard_input.just_pressed(KeyCode::F3) {
        dev_events.send(res::DeveloperCommandEvent {
            command: "spawn_powerup".to_string(),
        });
    }
}

/// Little developer helper, that can do developerish things.
pub fn developer_executive_system(
    mut state: ResMut<res::DeveloperExecutiveState>,
    dev_events: Res<Events<res::DeveloperCommandEvent>>,
    cursor_events: Res<Events<CursorMoved>>,
    bounds: Res<res::GameBounds>,
    mut spawn_events: ResMut<Events<res::SpawnEntityEvent>>,
) {
    let cursor_change = state.cursor_event_reader.latest(&cursor_events);
    if let Some(evt) = cursor_change {
        state.last_cursor_pos = evt.position;
    }
    for event in state.developer_event_reader.iter(&dev_events) {
        if event.command == "spawn_asteroid" {
            let mut rng = rand::thread_rng();
            use rand::Rng;

            let vx: f32 = rng.gen_range(-10.0, 10.0);
            let vy: f32 = rng.gen_range(-10.0, 10.0);

            let tx: f32 = rng.gen_range(
                bounds.point.x() - bounds.dimension.x() / 2.0,
                bounds.point.x() + bounds.dimension.x() / 2.0,
            );
            let ty: f32 = rng.gen_range(
                bounds.point.y() - bounds.dimension.y() / 2.0,
                bounds.point.y() + bounds.dimension.y() / 2.0,
            );

            let event = res::SpawnEntityEvent {
                spawn_type: res::SpawnType::Asteroid,
                location: Vec3::new(tx, ty, 0.0),
                velocity: Some(comp::physics::Velocity::new(vx, vy)),
                origin: None,
            };
            spawn_events.send(event);
        } else if event.command == "spawn_projectile" {
            let (tx, ty) = state.last_cursor_pos.into();

            let event = res::SpawnEntityEvent {
                spawn_type: res::SpawnType::Projectile,
                location: Vec3::new(tx, ty, 2.0),
                velocity: Some(comp::physics::Velocity::new(0.0, 80.0)),
                origin: None,
            };
            spawn_events.send(event);
        } else if event.command == "spawn_powerup" {
            let mut rng = rand::thread_rng();
            use rand::Rng;

            let tx: f32 = rng.gen_range(
                bounds.point.x() - bounds.dimension.x() / 2.0,
                bounds.point.x() + bounds.dimension.x() / 2.0,
            );
            let ty: f32 = bounds.point.y() + bounds.dimension.y() * 0.8;

            let event = res::SpawnEntityEvent {
                spawn_type: res::SpawnType::Powerup,
                location: Vec3::new(tx, ty, 1.0),
                velocity: Some(comp::physics::Velocity::new(0.0, -40.0)),
                origin: None,
            };
            spawn_events.send(event);
        } else {
            println!("Unknown developer command \'{}\'", event.command)
        }
    }
}

/// Keeps player within bounds defined as a resource
pub fn keep_player_in_bounds_system(
    bounds: Res<res::GameBounds>,
    mut query: Query<(
        &comp::actor::Player,
        &mut comp::physics::Velocity,
        &mut Transform,
    )>,
) {
    let half_x_bound = bounds.dimension.x() / 2.0;
    let half_y_bound = bounds.dimension.y() / 2.0;
    for (_, mut velocity, mut transform) in &mut query.iter() {
        let mut translation = transform.translation();

        if translation.x() > bounds.point.x() + half_x_bound {
            *translation.x_mut() = bounds.point.x() + half_x_bound;
            *velocity.x_mut() = 0.0;
        } else if translation.x() < bounds.point.x() - half_x_bound {
            *translation.x_mut() = bounds.point.x() - half_x_bound;
            *velocity.x_mut() = 0.0;
        }

        if translation.y() > bounds.point.y() + half_y_bound {
            *translation.y_mut() = bounds.point.y() + half_y_bound;
            *velocity.y_mut() = 0.0;
        } else if translation.y() < bounds.point.y() - half_y_bound {
            *translation.y_mut() = bounds.point.y() - half_y_bound;
            *velocity.y_mut() = 0.0;
        }

        transform.set_translation(translation);
    }
}

/// Prepares global spawner
///
/// TODO: This is a hack. It should be removed.
pub fn generic_spawn_entity_startup_system(
    mut commands: Commands,
    mut state: ResMut<res::GenericSpawnEntityState>,
) {
    let representative = commands
        .spawn(spawn::Spawner::default())
        .current_entity()
        .unwrap();
    state.representative = Some(representative);
}

/// Spawns things on request
///
/// TODO: There is an entity required to spawn some things.
/// This "global spawner" is nothing more than a hack for testing.
/// This entire system should be redesigned eventually.
pub fn generic_spawn_entity_system(
    mut commands: Commands,
    events: Res<Events<res::SpawnEntityEvent>>,
    mut state: ResMut<res::GenericSpawnEntityState>,
    asset_server: Res<AssetServer>,
    //materials: Res<res::ColorMaterialStorage>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    for event in state.event_reader.iter(&events) {
        match event.spawn_type {
            res::SpawnType::Asteroid => {
                let mut data = spawn::AsteroidDataComponents {
                    transform: Transform::from_translation(event.location),
                    ..Default::default()
                };
                if let Some(vel) = &event.velocity {
                    data.velocity = vel.clone();
                }
                spawn::spawn_asteroid(&mut commands, &asset_server, &mut materials, data);
            }
            res::SpawnType::Projectile => {
                let mut data = spawn::ProjectileDataComponents {
                    transform: Transform::from_translation(event.location),
                    ..Default::default()
                };
                if let Some(vel) = &event.velocity {
                    data.velocity = vel.clone();
                }
                spawn::spawn_projectile(
                    &mut commands,
                    &asset_server,
                    &mut materials,
                    state.representative.unwrap(),
                    data,
                );
            }
            res::SpawnType::Powerup => {
                let mut data = spawn::PowerupDataComponents {
                    transform: Transform::from_translation(event.location),
                    ..Default::default()
                };
                if let Some(vel) = &event.velocity {
                    data.velocity = vel.clone();
                }

                // Randomly select PowerUp kind.
                let kind = spawn::get_random_powerup_kind();

                spawn::spawn_powerup(
                    &mut commands,
                    &asset_server,
                    &mut materials,
                    data,
                    vec![kind]
                );
            }
        }
    }
}

/// Periodically spawns asteroids and powerups and "produces gameplay".
///
/// NOTE: This is a **purely placeholder system**, only for primitive gameplay/testing.
pub fn gameplay_spawn_system(
    mut commands: Commands,
    time: Res<Time>,
    bounds: Res<res::GameBounds>,
    mut state: ResMut<res::GameplaySpawnState>,
    asset_server: Res<AssetServer>,
    //materials: Res<res::ColorMaterialStorage>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    // Stop spawning
    if state.game_over {
        return;
    }

    let spawn_ypos_threshold = 100.0;
    let mut rng = rand::thread_rng();
    use rand::Rng;

    if state.asteroid_timer.just_finished {
        let tx: f32 = rng.gen_range(
            bounds.point.x() - bounds.dimension.x() / 2.0,
            bounds.point.x() + bounds.dimension.x() / 2.0,
        );
        let ty = bounds.point.y() + bounds.dimension.y() / 2.0 + spawn_ypos_threshold;

        // adjust "falling" speeds based on game time
        let base_speed = -100.0;
        let mut speed = (state.game_time / 80.0) * base_speed;
        if speed > base_speed {
            speed = base_speed
        } else if speed < base_speed * 6.0 {
            speed = base_speed * 6.0;
        }
        let vel = comp::physics::Velocity(Vec2::new(0.0, speed));

        let data = spawn::AsteroidDataComponents {
            transform: Transform::from_translation(Vec3::new(tx, ty, 0.0)),
            velocity: vel,
            ..Default::default()
        };

        spawn::spawn_asteroid(&mut commands, &asset_server, &mut materials, data);
    }

    if state.powerup_timer.just_finished {
        let tx: f32 = rng.gen_range(
            bounds.point.x() - bounds.dimension.x() / 2.0,
            bounds.point.x() + bounds.dimension.x() / 2.0,
        );
        let ty = bounds.point.y() + bounds.dimension.y() / 2.0 + spawn_ypos_threshold;

        // adjust "falling" speeds based on game time
        let base_speed = -80.0;
        let mut speed = (state.game_time / 85.0) * base_speed;
        if speed > base_speed {
            speed = base_speed
        } else if speed < base_speed * 6.0 {
            speed = base_speed * 6.0;
        }
        let vel = comp::physics::Velocity(Vec2::new(0.0, speed));

        let data = spawn::PowerupDataComponents {
            transform: Transform::from_translation(Vec3::new(tx, ty, 1.0)),
            velocity: vel,
            ..Default::default()
        };

        // Randomly select PowerUp kind.
        let kind = spawn::get_random_powerup_kind();

        spawn::spawn_powerup(
            &mut commands,
            &asset_server,
            &mut materials,
            data,
            vec![kind]
        );
    }

    state.asteroid_timer.tick(time.delta_seconds);
    state.powerup_timer.tick(time.delta_seconds);
    state.game_time += time.delta_seconds;
}

/// Removes all AutoCleaned entities that leave bottom part of GameBounds
pub fn off_bounds_cleanup_system(
    mut commands: Commands,
    bounds: Res<res::GameBounds>,
    mut query: Query<(Entity, &comp::AutoCleaned, &Transform)>,
) {
    let threshold = 650.0; // To prevent "on-screen" cleanup. TODO: This is a hack.
    let half_y_bound = bounds.dimension.y() / 2.0;
    for (ent, _, transform) in &mut query.iter() {
        if transform.translation().y() < bounds.point.y() - half_y_bound - threshold {
            commands.despawn(ent);
        }
    }
}

/// Checks if the game is over.
/// The game is over if the player is dead.
pub fn check_game_over_system(
    mut game_state: ResMut<res::GameplaySpawnState>,
    mut query: Query<(&comp::actor::Player, &comp::stats::CanDie)>,
) {
    if game_state.game_over {
        return;
    }

    for (_, death) in &mut query.iter() {
        if death.is_dead {
            game_state.game_over = true;
        }
    }
}
